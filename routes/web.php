<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/users/','UserController@index')->name('users.index');
Route::post('/users/search','UserController@search')->name('users.search');


Route::get('/error-test', function() {
    Log::info('calling the error route');
   throw new \Exception('something unexpected broke');
    return view('errors.500');

});
