@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>
                    <h3 class="page-title text-center">User found: {{ $user->name }}</h3>
                    <b>Email</b>: {{ $user->email }}
                    <br>
                    Last Login : {{$user->last_seen}}
                    <br>
                    <b>Registered on</b>: {{ $user->created_at }}
                </div>
            </div>
        </div>
    </div>
@endsection
