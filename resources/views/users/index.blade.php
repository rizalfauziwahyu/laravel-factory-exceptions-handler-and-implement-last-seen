@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>
                    @section('title', 'Error')

                    @section('message', 'Whoops, looks like something went wrong.'.(isset($incidentCode) ? ' Please contact support with incident code: '.$incidentCode : ''))
                    <div class="card-body">
                            @if (session('error'))
                                <div class="alert alert-danger">{{ session('error') }}</div>
                            @endif
                    </div>
                   <div class="card">
                       <div class="card-panel">
                           Search
                       </div>
                       <div class="card-body col-4 offset-4">
                           <form action="{{ route('users.search') }}" method="POST">
                               @csrf
                               <div class="form-group">
                                   <input id="user_id" class="form-control" name="user_id" type="text" value="{{ old('user_id') }}" placeholder="User ID">
                               </div>
                               <input class="btn btn-info" type="submit" value="Search">
                           </form>
                       </div>
                   </div>
                </div>
            </div>
        </div>
    </div>
@endsection
