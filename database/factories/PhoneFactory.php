<?php

use Faker\Generator as Faker;
$factory->define(App\Phones::class, function (Faker $faker) {
    return [
        'brand' => $faker->numerify('Samsung ###'),
        'model' => array_random([$faker->asciify('Infinity ***'),null]),
        'user_id' => App\User::inRandomOrder()->first()->id,
    ];
});


