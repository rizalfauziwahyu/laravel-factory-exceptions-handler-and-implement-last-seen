<?php

namespace App\Services;

use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Exceptions\UserNotFoundException;

class UserService
{
    public function search($user_id)
    {
        $user = User::find($user_id);
        if(!$user){
            throw new UserNotFoundException('User not found by ID '.$user_id);
        }
        return $user;
    }
}
