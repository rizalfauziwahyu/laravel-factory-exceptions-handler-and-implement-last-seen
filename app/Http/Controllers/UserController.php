<?php

namespace App\Http\Controllers;
use App\Exceptions\UserNotFoundException;
use App\User;
use Illuminate\Http\Request;
use App\Services\UserService;

class UserController extends Controller
{
    private $userServices;

    public function __construct(UserService $userService)
    {
        $this->middleware('auth');
        $this->userService = $userService;
    }

    public function index()
    {
        return view('users.index');
    }
    public function search(Request $request)
    {
        try{
            $user = $this->userService->search($request->input('user_id'));
        }catch (UserNotFoundException $exception){
            return back()->withError($exception->getMessage())->withInput();
        }
        return view('users.search',compact('user'));
    }
}
