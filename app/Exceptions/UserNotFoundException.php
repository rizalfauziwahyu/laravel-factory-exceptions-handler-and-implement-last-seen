<?php

namespace App\Exceptions;

use Exception;

class UserNotFoundException extends Exception
{
    //
    public function report()
    {
        \Log::debug('Log User Not Found');
    }

    public function render(Exception $exception)
    {
        return parent::render($exception);
    }
}
